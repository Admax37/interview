export class Answer {
	public answerText: string;
	public isCorrect: boolean;
}