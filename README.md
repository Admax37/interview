# Angular Questionnaire Project

## Abstract

Small project to test the familiarity of a candidate on the angular workflow.

This project contains a simple **list-details** functionality.

The **HTTP API** to be used can be inspected and tested [here](https://printer.ydrogiosonline.gr/swagger/index.html) on the **[Interviews]** section.

The **endpoints** of interest are the following:

- *​/api​/Interviews​/questions*
- *​/api​/Interviews​/answers/{question-id}*

The **models** of interest are the following:

- [Question](angular_questionnaire/src/models/question.ts)
- [Answer](angular_questionnaire/src/models/answer.ts)

Apart from the service and the two components listed bellow, no further work is required on the project.

The necessary parts that need to be completed are listed on the next sections.

## Questionnaire Service

The service file that should be completed can be found [here.](angular_questionnaire/src/app/services/questionnaire/questionnaire.service.ts)

This service should use **RxJS** and the **Angular HTTP client** to implement the following functionalities:

- Get the questions list from the API.
- Get the answers list from the API for a given **questionId**.

## Questions Component

The component files that should be completed can be found [here.](angular_questionnaire/src/app/components/questions)

This component should use the **Questionnaire Service** in order to show the questions in a table.

Each **Question** row should have the following columns:

- Question Text
- Button that loads the corresponding answers

## Answers Component

The component files that should be completed can be found [here.](angular_questionnaire/src/app/components/answers)

This component should use the **Questionnaire Service** 
in order to show the answers in a list, corresponding to the question that was selected.

Further specifications:

- The answers should be shown as a list of **AnswerText**.
- The answers should be sorted **lexicographically**.
- The column **isCorrect** of the **Answer** entity should be used to colorize the **AnswerText**, 
**green** for `true`, **red** for `false`.

## Styling

Styling is optional, but will be appreciated. Feel free to include any of the following libraries:

- Bootstrap
- Font Awesome
- Milligram
